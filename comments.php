<?php $args = array(
    'fields' => array (
        'author' => '<input id="author" type="text" class="form-control" placeholder="' . __('* Name') . '" name="author" required="required">',
        'email' => '<input id="email" type="email" class="form-control" placeholder="' . __('* Email') . '" name="email" required="required">',
        'url' => null
    ),
    'comment_field' => '<textarea id="comment" class="form-control" placeholder="' . _x( '* Comment', 'noun' ) . '" rows="5" name="comment" required="required"></textarea>',
    'class_form' => 'blog-comment-form',
    'id_form' => 'commentform',
    'title_reply' => __('Leave a comment'),
    'title_reply_before' => '<h3>',
    'title_reply_after' => '</h3>',
    'label_submit' => __('Submit a comment'),
    'class_submit' => 'form-control',
    'submit_button' => '<input name="submit" type="submit" class="%3$s" id="submit" value="%4$s">',
    'submit_field' => '<div class="contact-submit">%1$s %2$s</div>'
); ?>

<?php if (have_comments()) : $comments = get_comments(array(
    'post_id' => $post->ID,
    'status' => 'approve'
)); ?>
<div class="blog-comment">
    <h3><?php echo __('Comments'); ?></h3>
    <?php foreach ($comments as $comment) { ?>
    <div class="media">
        <div class="media-object pull-left">
            <?php echo get_avatar($comment->comment_author_email, 90); ?>
        </div>
        <div class="media-body">
            <h4 class="media-heading"><?php echo $comment->comment_author; ?></h4>
            <h5><?php $date = new DateTime($comment->comment_date); echo $date->format('d-M-Y'); ?></h5>
            <p><?php echo $comment->comment_content; ?></p>
        </div>
    </div>
    <?php } ?>

    <?php comment_form( $args, $post->ID ); ?>

<?php elseif (!have_comments()) : comment_form($args, $post->ID); ?>

<?php  endif; ?>
