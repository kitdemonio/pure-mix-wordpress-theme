<?php get_header(); ?>

    <!-- Header section
    ================================================== -->
    <section id="header" class="header-three">
        <div class="container">
            <div class="row">

                <div class="col-md-offset-3 col-md-6 col-sm-offset-2 col-sm-8">
                    <div class="header-thumb">
                        <h1 class="wow fadeIn" data-wow-delay="0.6s"><?php
                            $pagename = get_query_var('pagename');
                            if (!$pagename && $id > 0) {
                                // If a static page is set as the front page, $pagename will not be set. Retrieve it from the queried object
                                $post = $wp_query->get_queried_object();
                                $pagename = $post->post_desc;
                            }
                            echo $pagename; ?></h1>
                        <h3 class="wow fadeInUp"
                            data-wow-delay="0.9s">
                            <?php // Get current page description
                            $page = $wp_query->get_queried_object();
                            $page_id = $page->ID;

                            $pages = get_pages( array(
                                'include' => $page_id
                            ) );
                            foreach ( $pages as $page ) {
                                if ( $page->post_content ) echo $page->post_content;
                            }
                            ?></h3>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <!-- About section
    ================================================== -->
    <section id="about">
        <div class="container">
            <div class="row">
                <?php if ( have_posts() ) : query_posts('p=43'); //Post ID
                    while (have_posts()) : the_post(); ?>

                        <div class="wow fadeInUp col-md-4 col-sm-5" data-wow-delay="1.3s">
                            <?php the_post_thumbnail(array(360,560), array('class' => 'img-responsive')); ?>
                            <h1><?php the_title(); ?></h1>
                            <?php the_content(); ?>
                        </div>

                    <? endwhile; endif; wp_reset_query(); ?>

                <?php if ( have_posts() ) : query_posts('p=46'); //Post ID
                    while (have_posts()) : the_post(); ?>

                        <div class="wow fadeInUp col-md-7 col-sm-7" data-wow-delay="1.6s">
                            <h1><?php the_title(); ?></h1>
                            <?php the_content(); ?>
                            <blockquote>
                                <?php echo get_post_meta($post->ID, 'blockquote', true); ?>
                            </blockquote>
                            <?php the_post_thumbnail(array(650,340), array('class' => 'img-responsive')); ?>
                        </div>

                    <? endwhile; endif; wp_reset_query(); ?>

                <div class="clearfix"></div>

                <?php if ( have_posts() ) : query_posts( array('post__in' => array(49, 51, 53), 'cat=about', 'order' => 'ASC' ) );
                    while (have_posts()) : the_post(); ?>

                        <div class="wow fadeInUp col-md-4 col-sm-6" data-wow-delay="0.4s">
                            <h1><?php the_title(); ?></h1>
                            <?php the_content(); ?>
                        </div>

                    <? endwhile; endif; wp_reset_query(); ?>

                <div class="clearfix"></div>

                <?php if ( have_posts() ) : query_posts( array('post__in' => array(55, 57), 'cat=about', 'order' => 'ASC' ) );
                    while (have_posts()) : the_post(); ?>

                        <div class="wow fadeInUp col-md-6 col-sm-6" data-wow-delay="1.4s">
                            <h1><?php the_title(); ?></h1>
                            <?php the_content(); ?>
                        </div>

                    <? endwhile; endif; wp_reset_query(); ?>

            </div>
        </div>
    </section>

<?php get_footer(); ?>