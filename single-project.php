<?php get_header(); ?>

<!-- Header section
================================================== -->
<section id="header" class="header-two">
    <div class="container">
        <div class="row">

            <div class="col-md-offset-3 col-md-6 col-sm-offset-2 col-sm-8">
                <div class="header-thumb">
                    <h1 class="wow fadeIn" data-wow-delay="1.6s"><?php the_title(); ?></h1>
                    <h3 class="wow fadeInUp" data-wow-delay="1.9s"><?php the_excerpt(); ?></h3>
                </div>
            </div>

        </div>
    </div>
</section>


<!-- Single Project section
================================================== -->
<section id="single-project">
    <div class="container">
        <div class="row">

            <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

                <div class="wow fadeInUp col-md-offset-1 col-md-3 col-sm-offset-1 col-sm-4" data-wow-delay="2.3s">
                    <div class="project-info">
                        <h4>Client</h4>
                        <p><?php echo get_post_meta($post->ID, 'pr_client', true); ?></p>
                    </div>
                    <div class="project-info">
                        <h4>Date</h4>
                        <p><?php echo get_post_meta($post->ID, 'pr_date', true); ?></p>
                    </div>
                    <div class="project-info">
                        <h4>Category</h4>
                        <p><?php
                            $parent_cat = get_cat_name(4);
                            $cats = get_the_category($post->ID);
                            if ( $cats ) {
                                foreach ( $cats as $cat) {
                                    if ($cat->name != $parent_cat) {
                                        echo ' ' . $cat->name;
                                    }
                                }
                            }
                            ?></p>
                    </div>
                </div>

                <div class="wow fadeInUp col-md-7 col-sm-7" data-wow-delay="2.6s">
                    <?php the_content(); ?>
                    <?php the_post_thumbnail('full', array('class' => 'img-responsive')) ?>
                </div>
            <?php endwhile; ?>

        </div>
    </div>
</section>

<?php get_footer(); ?>,