<?php get_header(); ?>

    <!-- Header section
    ================================================== -->
    <section id="header" class="header-one">
        <div class="container">
            <div class="row">

                <div class="col-md-offset-3 col-md-6 col-sm-offset-2 col-sm-8">
                    <div class="header-thumb">
                        <h1 class="wow fadeIn"
                            data-wow-delay="1.6s"><?php if (is_front_page()) : echo get_bloginfo('name'); endif; ?></h1>
                        <h3 class="wow fadeInUp"
                            data-wow-delay="1.9s"><?php if (is_front_page()) : echo get_bloginfo('description'); endif; ?></h3>
                    </div>
                </div>

            </div>
        </div>
    </section>


    <!-- Portfolio section
    ================================================== -->
    <section id="portfolio">
        <div class="container">
            <div class="row">

                <div class="col-md-12 col-sm-12">

                    <!-- iso section -->
                    <div class="iso-section wow fadeInUp" data-wow-delay="2.6s">

                        <ul class="filter-wrapper clearfix">
                            <li><a href="#" data-filter="*" class="selected opc-main-bg">All</a></li>
                            <?php
                            $cats = get_categories( array(
                                'orderby' => 'name',
                                'parent'  => 4
                            ) );
                            if ( $cats ) {
                                foreach ( $cats as $cat) {
                                    if ( $cat->name ) {
                                        echo '<li><a href="#" class="opc-main-bg" data-filter=".' . strtolower($cat->name) . '">' . $cat->name . '</a></li>';
                                    }
                                }
                            } ?>
                        </ul>

                        <!-- iso box section -->
                        <div class="iso-box-section wow fadeInUp" data-wow-delay="1s">
                            <div class="iso-box-wrapper col4-iso-box">

                                <?php if (have_posts()) : query_posts(array('cat' => 4 | 'portfolio', 'post_type' => 'project', 'order' => 'ASC'));
                                    while (have_posts()) : the_post(); ?>

                                        <div class="iso-box col-md-4 col-sm-6 <?php
                                        $parent_cat = get_cat_name(4);
                                        $cats = get_the_category($post->ID);
                                        if ( $cats ) {
                                            foreach ( $cats as $cat) {
                                               if ($cat->name != $parent_cat) {
                                                   echo ' ' . strtolower($cat->name);
                                               }
                                            }
                                        }

                                        ?> ">
                                            <div class="portfolio-thumb">
                                                <?php if (has_post_thumbnail()) : the_post_thumbnail('full', array('class' => 'img-responsive')); endif; ?>
                                                <div class="portfolio-overlay">
                                                    <div class="portfolio-item">
                                                        <a href="<?php echo get_post_permalink(); ?>"><i
                                                                class="fa fa-link"></i></a>
                                                        <h2><?php the_title(); ?></h2>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    <? endwhile; endif;
                                wp_reset_query(); ?>

                            </div>
                        </div>

                    </div>

                </div>

            </div>
        </div>
    </section>

<?php get_footer(); ?>