<?php get_header(); ?>

<!-- Header section
================================================== -->
<section id="header" class="header-five">
    <div class="container">
		<div class="row">

			<div class="col-md-offset-3 col-md-6 col-sm-offset-2 col-sm-8">
          <div class="header-thumb">
              <h1 class="wow fadeIn" data-wow-delay="0.6s"><?php
                  $pagename = get_query_var('pagename');
                  if ( !$pagename && $id > 0 ) {
                      // If a static page is set as the front page, $pagename will not be set. Retrieve it from the queried object
                      $post = $wp_query->get_queried_object();
                      $pagename = $post->post_desc;
                  }
                  echo $pagename; ?></h1>
              <h3 class="wow fadeInUp" data-wow-delay="0.9s">
                  <?php // Get current page description
                  $page = $wp_query->get_queried_object();
                  $page_id = $page->ID;

                  $pages = get_pages( array(
                      'include' => $page_id
                  ) );
                  foreach ( $pages as $page ) {
                      if ( $page->post_content ) echo $page->post_content;
                  }
                  ?>
              </h3>
          </div>
			</div>

		</div>
	</div>
</section>


<!-- Blog section
================================================== -->
<section id="blog">
   <div class="container">
      <div class="row">

          <?php if ( have_posts() ) : query_posts('cat=p_blog&order=DESC'); $posts_count = 0;
              while (have_posts()) : the_post(); ?>
                  <?php $posts_count++; ?>

                  <?php if ( $posts_count <=2 ) : ?>

                      <div class="wow fadeInUp col-md-6 col-sm-6" data-wow-delay="1s">
                          <div class="blog-thumb">
                              <?php if ( has_post_thumbnail() ) : ?>
                                  <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                                      <?php the_post_thumbnail('full', array('class' => 'img-responsive')); ?>
                                  </a>
                              <?php endif; ?>
                              <a href="<?php the_permalink(); ?>"><h1><?php the_title(); ?></h1></a>
                              <div class="post-format">
                                  <span>By <?php the_author(); ?></span>
                                  <span><i class="fa fa-date"></i><?php echo get_the_date(); ?></span>
                              </div>
                              <?php the_excerpt(); ?>
                              <a href="<?php the_permalink(); ?>" class="btn btn-default"><?php echo __('Read More'); ?></a>
                          </div>
                      </div>
                  <?php endif; ?>
                  <?php if ( $posts_count > 2 && $posts_count <= 5 ) : ?>

                      <div class="wow fadeInUp col-md-4 col-sm-4" data-wow-delay="1.9s">
                          <div class="blog-thumb">
                              <?php if ( has_post_thumbnail() ) : ?>
                                  <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                                      <?php the_post_thumbnail(array(340,240), array('class' => 'img-responsive')); ?>
                                  </a>
                              <?php endif; ?>
                              <a href="<?php the_permalink(); ?>"><h1><?php the_title(); ?></h1></a>
                              <div class="post-format">
                                  <span>By <?php the_author(); ?></span>
                                  <span><i class="fa fa-date"></i><?php echo get_the_date(); ?></span>
                              </div>
                              <?php the_excerpt(); ?>
                              <a href="<?php the_permalink(); ?>" class="btn btn-default"><?php echo __('Read More'); ?></a>
                          </div>
                      </div>

                  <?php endif; ?>

                  <?php if ($posts_count > 5 ) : ?>

                      <div class="wow fadeInUp col-md-3 col-sm-3" data-wow-delay="2.4s">
                          <div class="blog-thumb">
                              <?php if ( has_post_thumbnail() ) : ?>
                                  <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                                      <?php the_post_thumbnail(array(260,175), array('class' => 'img-responsive')); ?>
                                  </a>
                              <?php endif; ?>
                              <a href="<?php the_permalink(); ?>"><h1><?php the_title(); ?></h1></a>
                              <div class="post-format">
                                  <span>By <?php the_author(); ?></span>
                                  <span><i class="fa fa-date"></i><?php echo get_the_date(); ?></span>
                              </div>
                              <?php the_excerpt(); ?>
                              <a href="<?php the_permalink(); ?>" class="btn btn-default"><?php echo __('Read More'); ?></a>
                          </div>
                      </div>

                  <?php endif; ?>

              <? endwhile; endif; wp_reset_query(); ?>

      </div>
   </div>
</section>

<?php get_footer(); ?>