<?php get_header(); ?>

<!-- Header section
================================================== -->
<section id="header" class="header-four">
	<div class="container">
		<div class="row">

			<div class="col-md-offset-3 col-md-6 col-sm-offset-2 col-sm-8">
            	<div class="header-thumb">
              		 <h1 class="wow fadeIn" data-wow-delay="0.6s"><?php
                         $pagename = get_query_var('pagename');
                         if ( !$pagename && $id > 0 ) {
                             // If a static page is set as the front page, $pagename will not be set. Retrieve it from the queried object
                             $post = $wp_query->get_queried_object();
                             $pagename = $post->post_desc;
                         }
                         echo $pagename; ?></h1>
              		 <h3 class="wow fadeInUp" data-wow-delay="0.9s">
                         <?php // Get current page description
                         $page = $wp_query->get_queried_object();
                         $page_id = $page->ID;

                         $pages = get_pages( array(
                             'include' => $page_id
                         ) );
                         foreach ( $pages as $page ) {
                             if ( $page->post_content ) echo $page->post_content;
                         }
                         ?>
                     </h3>
           		</div>
			</div>

		</div>
	</div>		
</section>


<!-- Contact section
================================================== -->
<?php $options = get_option( 'sample_theme_options' ); ?>
<section id="contact">
   <div class="container">
      <div class="row">

         <div class="wow fadeInUp col-md-6 col-sm-12" data-wow-delay="1.3s">
         	<div class="google_map">
<!--				<div id="map-canvas"></div>-->
                <iframe width="100%" height="300" frameborder="0" style="border:0;margin-top:40px;"
                        src="https://www.google.com/maps/embed/v1/place?q=<?php echo $options['input_address'] ?>&key=AIzaSyAEN8dFBkpVBjLtJA4iByy4s_TIDz8I2Hk"
                        allowfullscreen></iframe>
			</div>
		</div>

		<div class="wow fadeInUp col-md-6 col-sm-12" data-wow-delay="1.6s">
			<h1>Let's work together!</h1>
			<div class="contact-form">
				<form id="contact-form" method="post" action="https://formspree.io/<?php echo $options['input_email']?>">
					<input name="name" type="text" class="form-control" placeholder="<?php echo __('Your Name'); ?>" required>
					<input name="email" type="email" class="form-control" placeholder="<?php echo __('Your Email'); ?>" required>
					<textarea name="message" class="form-control" placeholder="<?php echo __('Message'); ?>" rows="4" required></textarea>
					<div class="contact-submit">
						<input type="submit" class="form-control submit" value="<?php echo __('Send a message'); ?>">
					</div>
				</form>
			</div>
		</div>

		<div class="clearfix"></div>
			<div class="col-md-4 col-sm-4">
				<div class="wow fadeInUp media" data-wow-delay="0.3s">
					<div class="media-object pull-left">
						<i class="fa fa-tablet"></i>
					</div>
					<div class="media-body">
						<h3 class="media-heading"><?php echo __('Phone'); ?></h3>
						<p><?php echo $options['input_phone']; ?></p>
					</div>
				</div>
			</div>

			<div class="col-md-4 col-sm-4">
				<div class="wow fadeInUp media" data-wow-delay="0.6s">
					<div class="media-object pull-left">
						<i class="fa fa-envelope"></i>
					</div>
					<div class="media-body">
						<h3 class="media-heading"><?php echo __('Email'); ?></h3>
						<p><?php echo $options['input_email']; ?></p>
					</div>
				</div>
			</div>

			<div class="col-md-4 col-sm-4">
				<div class="wow fadeInUp media" data-wow-delay="0.9s">
					<div class="media-object pull-left">
						<i class="fa fa-globe"></i>
					</div>
					<div class="media-body">
						<h3 class="media-heading"><?php echo __('Address'); ?></h3>
						<p><?php echo $options['input_address']; ?></p>
					</div>
				</div>
			</div>

      </div>
   </div>
</section>

<?php get_footer(); ?>