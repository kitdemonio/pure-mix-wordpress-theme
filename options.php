<?php

add_action( 'admin_init', 'theme_options_init' );
add_action( 'admin_menu', 'theme_options_add_page' );

/**
 * Init plugin options to white list our options
 */
function theme_options_init(){
    register_setting( 'sample_options', 'sample_theme_options', 'theme_options_validate' );
}

/**
 * Load up the menu page
 */
function theme_options_add_page() {
    add_theme_page( __( 'Theme Customization', 'puremix' ), __( 'Theme Customization', 'puremix' ), 'edit_theme_options', 'theme_options', 'theme_options_do_page' );
}

/**
 * Create arrays for our select and radio options
 */

$radio_options = array(
    'yes' => array(
        'value' => 'yes',
        'label' => __( 'Yes', 'puremix' )
    ),
    'no' => array(
        'value' => 'no',
        'label' => __( 'No', 'puremix' )
    )
);

/**
 * Create the options page
 */
function theme_options_do_page() {
    global $select_options, $radio_options;

    if ( ! isset( $_REQUEST['settings-updated'] ) )
        $_REQUEST['settings-updated'] = false;

    ?>
    <div class="wrap">
        <?php screen_icon(); echo "<h2>" . get_current_theme() . __( ' Theme Options', 'puremix' ) . "</h2>"; ?>

        <?php if ( false !== $_REQUEST['settings-updated'] ) : ?>
            <div class="updated fade"><p><strong><?php _e( 'Options saved', 'puremix' ); ?></strong></p></div>
        <?php endif; ?>

        <form method="post" action="options.php">
            <?php settings_fields( 'sample_options' ); ?>
            <?php $options = get_option( 'sample_theme_options' ); ?>

            <table class="form-table">

                <?php
                /**
                 * A sample checkbox option
                 */
                ?>
<!--                <tr valign="top"><th scope="row">--><?php //_e( 'A checkbox', 'puremix' ); ?><!--</th>-->
<!--                    <td>-->
<!--                        <input id="sample_theme_options[option1]" name="sample_theme_options[option1]" type="checkbox" value="1" --><?php //checked( '1', $options['option1'] ); ?><!-- />-->
<!--                        <label class="description" for="sample_theme_options[option1]">--><?php //_e( 'Sample checkbox', 'puremix' ); ?><!--</label>-->
<!--                    </td>-->
<!--                </tr>-->

                <?php
                /**
                 * A text input options
                 */
                ?>
                <tr>
                    <td><h3 style="text-transform: uppercase;font-size: 16px;">Contact information</h3></td>
                </tr>
                <tr valign="top"><th scope="row"><?php _e( 'Company name', 'puremix' ); ?></th>
                    <td>
                        <input id="sample_theme_options[input_name]" class="regular-text" type="text" name="sample_theme_options[input_name]" placeholder="Hello Company" value="<?php esc_attr_e( $options['input_name'] ); ?>" />
                    </td>
                </tr>
                <tr valign="top"><th scope="row"><?php _e( 'Phone', 'puremix' ); ?></th>
                    <td>
                        <input id="sample_theme_options[input_phone]" class="regular-text" type="text" name="sample_theme_options[input_phone]" placeholder="+99 00 8877 6655" value="<?php esc_attr_e( $options['input_phone'] ); ?>" />
                    </td>
                </tr>
                <tr valign="top"><th scope="row"><?php _e( 'Address', 'puremix' ); ?></th>
                    <td>
                        <input id="sample_theme_options[input_address]" class="regular-text" type="text" name="sample_theme_options[input_address]" placeholder="123 New Street, Old Town Another Village, 11220" value="<?php esc_attr_e( $options['input_address'] ); ?>" />
                    </td>
                </tr>
                <tr valign="top"><th scope="row"><?php _e( 'Email', 'puremix' ); ?></th>
                    <td>
                        <input id="sample_theme_options[input_email]" class="regular-text" type="text" name="sample_theme_options[input_email]" placeholder="hello@company.com" value="<?php esc_attr_e( $options['input_email'] ); ?>" />
                    </td>
                </tr>
                <tr>
                    <td><h3 style="text-transform: uppercase;font-size: 16px;">Social accounts</h3></td>
                </tr>
                <!--      Social links          -->
                <tr valign="top"><th scope="row"><?php _e( 'Facebook', 'puremix' ); ?></th>
                    <td>
                        <input id="sample_theme_options[input_sc_facebook]" class="regular-text" type="text" name="sample_theme_options[input_sc_facebook]"  value="<?php esc_attr_e( $options['input_sc_facebook'] ); ?>" />
                    </td>
                </tr>
                <tr valign="top"><th scope="row"><?php _e( 'Twitter', 'puremix' ); ?></th>
                    <td>
                        <input id="sample_theme_options[input_sc_twitter]" class="regular-text" type="text" name="sample_theme_options[input_sc_twitter]"  value="<?php esc_attr_e( $options['input_sc_twitter'] ); ?>" />
                    </td>
                </tr>
                <tr valign="top"><th scope="row"><?php _e( 'Dribbble', 'puremix' ); ?></th>
                    <td>
                        <input id="sample_theme_options[input_sc_ddribbble]" class="regular-text" type="text" name="sample_theme_options[input_sc_dribbble]"  value="<?php esc_attr_e( $options['input_sc_dribbble'] ); ?>" />
                    </td>
                </tr>
                <tr valign="top"><th scope="row"><?php _e( 'Behance', 'puremix' ); ?></th>
                    <td>
                        <input id="sample_theme_options[input_sc_behance]" class="regular-text" type="text" name="sample_theme_options[input_sc_behance]"  value="<?php esc_attr_e( $options['input_sc_behance'] ); ?>" />
                    </td>
                </tr>
                <tr valign="top"><th scope="row"><?php _e( 'Google plus', 'puremix' ); ?></th>
                    <td>
                        <input id="sample_theme_options[input_sc_google]" class="regular-text" type="text" name="sample_theme_options[input_sc_google]"  value="<?php esc_attr_e( $options['input_sc_google'] ); ?>" />
                    </td>
                </tr>

            <p class="submit">
                <input type="submit" class="button-primary" value="<?php _e( 'Save Options', 'puremix' ); ?>" />
            </p>
        </form>
    </div>
    <?php
}

/**
 * Sanitize and validate input. Accepts an array, return a sanitized array.
 */
function theme_options_validate( $input ) {
    global $radio_options;

    // Our checkbox value is either 0 or 1
    if ( ! isset( $input['option1'] ) )
        $input['option1'] = null;
    $input['option1'] = ( $input['option1'] == 1 ? 1 : 0 );

    // Say our text option must be safe text with no HTML tags
    $input['input_name'] = wp_filter_nohtml_kses( $input['input_name'] );
    $input['input_email'] = wp_filter_nohtml_kses( $input['input_email'] );
    $input['input_phone'] = wp_filter_nohtml_kses( $input['input_phone'] );
    $input['input_address'] = wp_filter_nohtml_kses( $input['input_address'] );
    $input['input_sc_facebook'] = wp_filter_nohtml_kses( $input['input_sc_facebook'] );
    $input['input_sc_twitter'] = wp_filter_nohtml_kses( $input['input_sc_twitter'] );
    $input['input_sc_dribbble'] = wp_filter_nohtml_kses( $input['input_sc_dribbble'] );
    $input['input_sc_behance'] = wp_filter_nohtml_kses( $input['input_sc_behance'] );
    $input['input_sc_google'] = wp_filter_nohtml_kses( $input['input_sc_google'] );

    // Our select option must actually be in our array of select options
//    if ( ! array_key_exists( $input['input_email'], $select_options ) )
//        $input['input_email'] = null;

    // Our radio option must actually be in our array of radio options
//    if ( ! isset( $input['radioinput'] ) )
//        $input['radioinput'] = null;
//    if ( ! array_key_exists( $input['radioinput'], $radio_options ) )
//        $input['radioinput'] = null;
//
//    // Say our textarea option must be safe text with the allowed tags for posts
//    $input['sometextarea'] = wp_filter_post_kses( $input['sometextarea'] );

    return $input;
}
