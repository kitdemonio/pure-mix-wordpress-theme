<?php get_header(); ?>
<?php
add_filter( 'the_content', 'pm_insert_featured_image', 40 ); // Insert post image between post content
function pm_insert_featured_image( $content ) { //
    if ( !get_query_var('pagename') ) {
        $content = preg_replace( '/<\/p>/', '</p>' . get_the_post_thumbnail($post->ID, 'post-image'), $content, 1 );
        return $content;
    } else {
        return $content;
    }
}
?>
<!-- Header section
================================================== -->
<section id="header" class="header-five">
	<div class="container">
		<div class="row">

			<div class="col-md-offset-3 col-md-6 col-sm-offset-2 col-sm-8">
          <div class="header-thumb">
              <h1 class="wow fadeIn" data-wow-delay="0.6s">Blog</h1>
              <h3 class="wow fadeInUp" data-wow-delay="0.9s"></h3>
          </div>
			</div>

		</div>
	</div>
</section>


<!-- Single Post section
================================================== -->
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
    <section id="single-post">
   <div class="container">
      <div class="row">

         <div class="wow fadeInUp col-md-offset-1 col-md-10 col-sm-offset-1 col-sm-10" data-wow-delay="2.3s">
         	  <div class="blog-thumb">

         		   <h1><?php the_title(); ?></h1>
         			    <div class="post-format">
						        <span>By <a href="#"><?php the_author(); ?></a></span>
						        <span><i class="fa fa-date"></i><?php the_date(); ?></span>
					       </div>
                  <?php if  (get_post_meta($post->ID, 'blockquote', true) ) { ?>
                  <blockquote>
                      <?php echo get_post_meta($post->ID, 'blockquote', true); ?>
                  </blockquote>
                  <?php }?>

                  <?php the_content(); ?>

                  <?php if ( comments_open() || get_comments_number() )
                      comments_template();
                  ?>
         	  </div>
		    </div>

      </div>
   </div>
</section>
<?php endwhile; ?>

<?php get_footer(); ?>