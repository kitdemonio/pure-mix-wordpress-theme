<!-- Footer section
================================================== -->
<footer>
    <div class="container">
        <div class="row">

            <div class="col-md-12 col-sm-12">
                <p class="wow fadeInUp"  data-wow-delay="0.3s">Copyright &copy; <?php $options = get_option( 'sample_theme_options' ); echo date('Y') . ' ' . $options['input_name']; ?> - Designed by Tooplate</p>
                <ul class="social-icon wow fadeInUp"  data-wow-delay="0.6s">
                    <?php if ( $options['input_sc_facebook'] ) : ?>
                    <li><a href="<?php echo $options['input_sc_facebook'] ?>" class="fa fa-facebook" target="_blank" title="Facebook"></a></li>
                    <?php endif; ?>

                    <?php if ( $options['input_sc_twitter'] ) : ?>
                    <li><a href="<?php echo $options['input_sc_twitter'] ?>" class="fa fa-twitter" target="_blank" title="Twitter"></a></li>
                    <?php endif; ?>

                    <?php if ( $options['input_sc_dribbble'] ) : ?>
                        <li><a href="<?php echo $options['input_sc_dribbble'] ?>" class="fa fa-dribbble" target="_blank" title="Dribbble"></a></li>
                    <?php endif; ?>

                    <?php if ( $options['input_sc_behance'] ) : ?>
                        <li><a href="<?php echo $options['input_sc_behance'] ?>" class="fa fa-behance" target="_blank" title="Behance"></a></li>
                    <?php endif; ?>

                    <?php if ( $options['input_sc_google'] ) : ?>
                        <li><a href="<?php echo $options['input_sc_google'] ?>" class="fa fa-google-plus" target="_blank" title="Google plus"></a></li>
                    <?php endif; ?>
                </ul>
            </div>

        </div>
    </div>
</footer>


<?php wp_footer();?>

</body>
</html>