<?php
// Register 'Project' type posts
add_action('init', 'register_post_types');
function register_post_types()
{
    register_post_type('project', array(
        'label' => null,
        'labels' => array(
            'name' => __('Projects'),
            'singular_name' => __('project'),
            'add_new' => __('Add new project'),
            'add_new_item' => __('Add project'),
            'edit_item' => __('Edit project'),
            'new_item' => __('New'),
            'view_item' => __('View'),
            'search_items' => __('Search'),
            'not_found' => __('Not found'),
            'not_found_in_trash' => __('Not found in trash'),
            'parent_item_colon' => '',
            'menu_name' => __('Projects'),
        ),
        'description' => '',
        'public' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => null,
        'menu_position' => 5,
        'menu_icon' => null,
        'capability_type'   => 'post',
        //'capabilities'      => 'post',
        //'map_meta_cap'      => null,
        'hierarchical' => false,
        'supports' => array('title','editor','author','thumbnail','excerpt','custom-fields','comments','revisions','page-attributes','post-formats'),
        'taxonomies' => array('category', 'post_tag'),
        'has_archive' => true,
        'rewrite' => true,
        'query_var' => true,
        'can_export' => true,
    ));
}

/*
 * Add styles
 */
add_action('wp_enqueue_scripts', 'enqueue_theme_styles');

function enqueue_theme_styles()
{
//    Bootstrap CSS
    wp_enqueue_style('bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css');
//    Animate css
    wp_enqueue_style('animate', get_template_directory_uri() . '/css/animate.min.css');
//    Font Awesome
    wp_enqueue_style('font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css');
//    Ionicons
    wp_enqueue_style('ionicons', get_template_directory_uri() . '/css/ionicons.min.css');
//    Main Styles
    wp_enqueue_style('main-style', get_template_directory_uri() . '/css/style.css');
};

/*
 * Register theme scripts
 * */
add_action('wp_enqueue_scripts', 'enqueue_theme_scripts');

function enqueue_theme_scripts() {
    wp_enqueue_script('bootstrap_min', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'));
    wp_enqueue_script('isotope', get_template_directory_uri() . '/js/isotope.js');
    wp_enqueue_script('imagesloaded', get_template_directory_uri() . '/js/imagesloaded.min.js');
    wp_enqueue_script('wow', get_template_directory_uri() . '/js/wow.min.js');
    wp_enqueue_script('custom', get_template_directory_uri() . '/js/custom.js', array('jquery'));
}

//Remove unwanted actions from wp_head();
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');

/*
 * Add menu support
 */
add_theme_support('menus');

/*
 * Add post-format support
 */
add_theme_support('post-formats');

/*
 * Add post-thumbnails support
 */
add_theme_support('post-thumbnails');

/*
 * Add widgets support
 */
//add_theme_support('widgets');

/*
 * Register Header Menu
 */
function register_my_menu()
{
    register_nav_menu('header-menu', __('Header Menu'));
    register_nav_menu('footer-menu', __('Footer Menu'));
}

add_action('init', 'register_my_menu');

/*
 * Filters
 */

function new_excerpt_length($length) { // Change excerpt length
    return 25;
}
add_filter('excerpt_length', 'new_excerpt_length');
add_filter('excerpt_more', function($more) {
    return '...';
});

add_filter('comment_form_fields', 'pm_reorder_comment_fields' ); //Reorder comment fields
function pm_reorder_comment_fields( $fields ){
    // die(print_r( $fields ));

    $new_fields = array();

    $myorder = array('author','email','url','comment');

    foreach( $myorder as $key ){
        $new_fields[ $key ] = $fields[ $key ];
        unset( $fields[ $key ] );
    }

    if( $fields )
        foreach( $fields as $key => $val )
            $new_fields[ $key ] = $val;

    return $new_fields;
}

require_once ( get_stylesheet_directory() . '/options.php' );
