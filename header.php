<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>

    <meta charset="<?php echo get_bloginfo('charset'); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="">
    <meta name="description" content="">

    <!-- Site title
   ================================================== -->
    <title><?php echo get_bloginfo('name'); ?></title>

    <!-- Google web font
   ================================================== -->
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700,300' rel='stylesheet' type='text/css'>
<?php wp_head(); ?>
</head>

<body>


<!-- Preloader section
================================================== -->
<!--<div class="preloader">-->
<!---->
<!--    <div class="sk-spinner sk-spinner-pulse"></div>-->
<!---->
<!--</div>-->


<!-- Navigation section
================================================== -->
<div class="nav-container">
    <nav class="nav-inner transparent">

        <div class="navbar">
            <div class="container">
                <div class="row">

                    <div class="brand">
                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php bloginfo('name'); ?></a>
                    </div>

                    <div class="navicon">
                        <div class="menu-container">

                            <div class="circle dark inline">
                                <i class="icon ion-navicon"></i>
                            </div>

                            <div class="list-menu">
                                <i class="icon ion-close-round close-iframe"></i>
                                <div class="intro-inner">
                                    <?php if (has_nav_menu('header-menu')) {
                                        wp_nav_menu( array(
                                            'header-menu' => 'header-menu',
                                            'menu_id' => 'nav-menu',
                                            'container' => ''
                                        ) );
                                    } ?>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>

    </nav>
</div>